# Changelog
All notable changes to this project will be documented in this file.

## 2021-05-18

### Added
- Add authentication request handlers
- Add authentication service app and app: not working
- Add .gitlab-ci.yml file - test login, build and push for the image to gitlab registry

## 2021-05-19
### Updated
- Split authentication service in signup and login 
### Added
- Add jwt token-based authentication at login
- Add token_required method for accessing reservation resources

## 2021-05-19

### Updated
- Cleanup code