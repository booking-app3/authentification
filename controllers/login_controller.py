from flask import Response, request, Blueprint, jsonify, make_response
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError
from guest import Guest
from utils import required_params
from session import Session
from  werkzeug.security import generate_password_hash, check_password_hash
import jwt
from os import environ as env

login_controller = Blueprint('login_controller', __name__, template_folder='controllers')
SECRET_KEY = 'basic-key-login'

@login_controller.route("/api/login", methods=['POST'])
@required_params({"username": str, "password": str})
def add_user():
    data = request.get_json(silent=True)
    session = Session()
    try:
        guest = session.query(Guest).filter_by(username=data["username"]).first()

    except NoResultFound:
        session.rollback()
        return Response(status=404)

    except Exception as e:
        session.rollback()
        return str(e)

    if check_password_hash(guest.password, data['password']):
        # generates the JWT Token
        token = jwt.encode({
            'public_id': guest.id
        }, SECRET_KEY)

        return make_response(jsonify({'token' : token}), 201)
    # returns 403 if password is wrong
    return make_response(
        'Could not verify',
        403,
        {'WWW-Authenticate' : 'Basic realm ="Wrong Password !!"'}
    )

@login_controller.route('/api/signup', methods = ['POST'])
@required_params({"username": str, "password": str, "email": str})
def signup():
    # creates a dictionary of the form data
    session = Session()
    data = request.get_json(silent=True)

    try:
        guest = Guest(
            email=data['email'],
            password=generate_password_hash(data['password']),
            username=data['username']
        )
        session.add(guest)
        session.commit()

    except IntegrityError:
        session.rollback()
        return Response(status=409)

    except NoResultFound:
        session.rollback()
        return Response(status=404)

    except Exception as e:
        session.rollback()
        return str(e)

    return make_response('Successfully registered.', 201)

@login_controller.teardown_request
def shutdown_session(exception=None):
    Session.remove()