from flask import Flask
from login_controller import login_controller
from utils import CustomJSONEncoder

app = Flask(__name__)
app.json_encoder = CustomJSONEncoder
app.register_blueprint(login_controller)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug = True, port=5001)